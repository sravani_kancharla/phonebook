﻿<?php
//database connection
session_start();
$errors = array();
$user = 'root';
$pass = '';
$db = 'register';
$conn = new mysqli('localhost', $user, $pass, $db) or die("Not able to connect");

//if($conn)
  //echo "Connection established";

// REGISTER USER
if (isset($_POST['reg'])) {
  // receive all input values from the form
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $password = mysqli_real_escape_string($conn, $_POST['password']);
  $password1 = mysqli_real_escape_string($conn, $_POST['password1']);

  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password)) { array_push($errors, "Password is required"); }
  if (empty($password1)) { array_push($errors, "Confirm Password is required"); }
  if ($password != $password1) {
	array_push($errors, "The two passwords do not match");
  }
  // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE email='$email' LIMIT 1";
  $result = mysqli_query($conn, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['email'] === $email) {
      array_push($errors, "Email already exists");
    }
  }
  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password1);//encrypt the password before saving in the database

  	$query = "INSERT INTO users (email, password)
  			  VALUES('$email', '$password')";
  	mysqli_query($conn, $query);
  	$_SESSION['email'] = $email;
  	//$_SESSION['success'] = "You are now logged in";
  	header('location: phone.php');
    }
  }

//login user
if (isset($_POST['log'])) {
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $password = mysqli_real_escape_string($conn, $_POST['password']);
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password)) { array_push($errors, "Password is required"); }
  if (count($errors) == 0) {
  	$password = md5($password);
  	$query = "SELECT * FROM users WHERE email='$email' AND password='$password'";
  	$results = mysqli_query($conn, $query);
  	if (mysqli_num_rows($results) == 1) {
  	   $_SESSION['email'] = $email;
  	   $_SESSION['success'] = "You are now logged in";
  	  header('location: phone.php');
        //echo "saa";
  	}else {
  		array_push($errors, "Wrong email/password combination");
  	}
  }
}

?>
