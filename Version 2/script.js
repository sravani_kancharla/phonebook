function editMode() {
  var edit_boxes = [ "edit_box1", "edit_box2", "edit_box3",
    "edit_box4", "edit_box5"]
  for (var i = 0; i < edit_boxes.length; i++) {
    var obj = document.getElementById(edit_boxes[i])
    obj.style.border = "1px solid black";
    obj.removeAttribute("readonly")
  }

  document.getElementById("edit_button").setAttribute("name", "update");
  // document.getElementById("update_form").submit();
}
