<!DOCTYPE html>
<html>
<div style="background-color:#4682B4; color:white; padding:10px; font-size: 80% ">
<h1 align="center" style="font-family:verdana" >Address book</></h1>
</div>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

</style>
<script src="script.js"></script>
</body>
<?php
include('index.php');

// add
if (isset($_POST['add'])) {
  // receive all input values from the form
  $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
  $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $phone = mysqli_real_escape_string($conn, $_POST['phone']);
  $address = mysqli_real_escape_string($conn, $_POST['address']);

  if (empty($firstname)) { array_push($errors, "Firstname is required"); }
  if (empty($lastname)) { array_push($errors, "Lastname is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($phone)) { array_push($errors, "Phonenumber is required"); }
  if (empty($address)) { array_push($errors, "Address is required"); }

 // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $query1 = "SELECT * FROM address WHERE firstname='$firstname' AND lastname='$lastname' AND email='$email' AND phone='$phone' AND address='$address' LIMIT 1";
  $result1 = mysqli_query($conn, $query1);
  $user1 = mysqli_fetch_assoc($result1);


  if ($user1) { // if user exists
    if ($user1['firstname'] === $firstname AND $user1['lastname'] === $lastname AND $user1['email'] === $email AND $user1['phone'] === $phone AND $user1['address'] === $address) {
      array_push($errors, "Same data already exists");
     }
   }

  if (count($errors) == 0) {
  	$query = "INSERT INTO address (firstname, lastname, email, phone, address)
  			  VALUES('$firstname', '$lastname', '$email', '$phone', '$address')";
  	mysqli_query($conn, $query);
  	//$_SESSION['email'] = $email;
  	//$_SESSION['success'] = "You are now logged in";
  	//header('location: add1.php');
        // echo $firstname or $lastname;
    }
 }

// delete
if (isset($_POST['delete'])) {
  $id = mysqli_real_escape_string($conn, $_POST['id']);
  if ($id) {
    $query = "UPDATE address set deleted = true where id = '$id'";
    mysqli_query($conn, $query);
  }
}

//$edit_id = 0;
if (isset($_POST['edit'])) {
  $edit_id = mysqli_real_escape_string($conn, $_POST['id']);
}

if(isset($_POST["restore_session"])) {
  $query = "SELECT * FROM address where deleted = true";
  $button = '<button type="submit" name="address_book">Back to address book</button>';
}
elseif (isset($_POST["address_book"])) {
  $query = "SELECT * FROM address where deleted = false";
  $button = '<button type="submit" name="restore_data">Restore addresses</button>';
}
?>
<center><h2>List of contents</h2></center>
<?php echo "$button" ?>
<table>
<tr>
<th>Firstname</th>
<th>Lastname</th>
<th>Email</th>
<th>Phone</th>
<th>Address</th>
<th colspan="3">Add</th>
</tr>
<tr>
<form method="post" action="phone.php">
 <td><input type="text" name="firstname"></td>
 <td><input type="text" name="lastname"></td>
 <td><input type="text" name="email"></td>
 <td><input type="text" name="phone"></td>
 <td><input type="text" name="address"></td>
 <td><button type="submit" name="add">add</button></td>
</form>
</tr>
<?php

$result = mysqli_query($conn, $query1);
while($row = mysqli_fetch_array($result))
   {
    ?>
    <!-- <form id="update_form" action="phone.php" method="post"> -->
      <tr>
        <td>
          <input id="edit_box1" type="text" name="firstname" readonly
            style="border:none" value="<?php echo $row['firstname'];?>">
        </td>
        <td>
          <input id="edit_box2" type="text" name="lastname" readonly
            style="border:none" value="<?php echo $row['lastname'];?>">
        </td>
        <td>
          <input id="edit_box3" type="text" name="email" readonly
             style="border:none" value="<?php echo $row['email'];?>">
        </td>
        <td>
          <input id="edit_box4" type="text" name="phone" readonly
            style="border:none" value="<?php echo $row['phone'];?>">
        </td>
        <td>
          <input id="edit_box5" type="text" name="address" readonly
            style="border:none" value="<?php echo $row['address'];?>">
        </td>
      </tr>
     <!-- </form> -->
     <td>
     <form action="phone.php" method="post">
       <button type="submit" name="delete">delete</button>
         <input type="hidden" name="id" value="<?php echo $row['id'];?>">
       </form>
       <button id="edit_button" type="submit" name="edit" onclick="editMode()">edit</button>
     </td>
    <?php } ?>
</table>
</html>
