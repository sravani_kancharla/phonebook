﻿<!DOCTYPE html>
<html>
<body style="background-image:url('01.jpg');">
<div style="background-color:#4682B4; color:white; padding:10px; font-size: 80% ">
<h1 align="center" style="font-family:verdana" >Address book</></h1>
</div>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

</style>
</body>
<?php
include('index.php');

// add
if (isset($_POST['add'])) {
  // receive all input values from the form
  $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
  $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $phone = mysqli_real_escape_string($conn, $_POST['phone']);
  $address = mysqli_real_escape_string($conn, $_POST['address']);

  if (empty($firstname)) { array_push($errors, "Firstname is required"); }
  if (empty($lastname)) { array_push($errors, "Lastname is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($phone)) { array_push($errors, "Phonenumber is required"); }
  if (empty($address)) { array_push($errors, "Address is required"); }

 // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $query1 = "SELECT * FROM address WHERE firstname='$firstname' AND lastname='$lastname' AND email='$email' AND phone='$phone' AND address='$address' LIMIT 1";
  $result1 = mysqli_query($conn, $query1);
  $user1 = mysqli_fetch_assoc($result1);


  if ($user1) { // if user exists
    if ($user1['firstname'] === $firstname AND $user1['lastname'] === $lastname AND $user1['email'] === $email AND $user1['phone'] === $phone AND $user1['address'] === $address) {
      array_push($errors, "Same data already exists");
     }
   }

  if (count($errors) == 0) {
  	$query = "INSERT INTO address (firstname, lastname, email, phone, address)
  			  VALUES('$firstname', '$lastname', '$email', '$phone', '$address')";
  	mysqli_query($conn, $query);
  	//$_SESSION['email'] = $email;
  	//$_SESSION['success'] = "You are now logged in";
  	//header('location: add1.php');
        // echo $firstname or $lastname;
    }
 }

// delete
if (isset($_POST['delete'])) {
  $id = mysqli_real_escape_string($conn, $_POST['id']);
  if ($id) {
    $query = "DELETE FROM address where id = '$id'";
    mysqli_query($conn, $query);
  }
}

?>
<center><h2>List of contents</h2></center>
<table>
<tr>
<th>Firstname</th>
<th>Lastname</th>
<th>Email</th>
<th>Phone</th>
<th>Address</th>
<th colspan="3">Add</th>
</tr>
<tr>
<form method="post" action="phone.php">
 <td><input type="text" name="firstname"></td>
 <td><input type="text" name="lastname"></td>
 <td><input type="text" name="email"></td>
 <td><input type="text" name="phone"></td>
 <td><input type="text" name="address"></td>
 <td width ="40%" colspan="2"><button type="submit" name="add">add</button></td>
</form>
</tr>
<?php
$edit_id = 0;
if (isset($_POST['edit'])) {
  $edit_id = mysqli_real_escape_string($conn, $_POST['id']);
}
$result = mysqli_query($conn, "SELECT * FROM address");
while($row = mysqli_fetch_array($result))
   {
    ?>
      <tr width="100%">
        <td> <?php echo $row['firstname'];  ?> </td>
        <td> <?php echo $row['lastname'];  ?> </td>
        <td> <?php echo $row['email'];  ?> </td>
        <td> <?php echo $row['phone'];  ?> </td>
        <td> <?php echo $row['address'];  ?> </td>
     <td>
       <form action="phone.php" method="post">
         <button type="submit" name="delete">delete</button>
         <button type="submit" name="edit">edit</edit>
         <input type="hidden" name="id" value="<?php echo $row['id'];?>">
       </form>
       </tr>
     </td>
    <?php } ?>
</table>
</html>
