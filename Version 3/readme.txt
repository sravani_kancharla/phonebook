This project provides user with a register form, anyone can able to register with the email and password and after registering a confirmation email is sent to the
user with confirmation message. 
After registering, user can able to loggin anytime with login form. 
After login, user is provided with a table having firstname, lastname, email, phone, address contents. User can enter any number of details and add them in to
the table and user can edit and delete the data. 
After the work is done, user can logout from the page by clicking the logout button.

The project is developed using html, css, php, sql
Files data:
1. register.php: The file provides all the registertion form requirements for the user to register.
2. login.php: This file provides the user with a login form and user can anytime login with email and password credentials
3. database.php: In this file database connection is established and email and password checking is executed. If all the credentials are matched then this file
redirects the user to phonebook webpage(phone.php) and this file is included in both register.php and login.php pages.
4. phone.php: This file provides the user with a table containing firstname, lastname, email, phone, address details and also with add, delete, edit buttons
inorder to add, delete, edit data. Logout option is also provided inorder to logout form the webpage.
5. index.php: This file contains database connection and all the table details are post to this file and this file executes the selection, insertion and deletion of data
6. logout.php: If the user is not logged in, this file redirects to the login page. Hence the main phone.php file page is accessible to only logged in users. This file 
also provides the user to logout
7. mail.php: This file contains all the requriments to sent a confirmation mail to the user
8. style.css: In this file all the styling attributes are included inorder to look the page more handy and colourful.

